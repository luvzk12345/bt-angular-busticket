import { Component, OnInit, ViewChild } from '@angular/core';
import { GheComponent } from '../ghe/ghe.component';

@Component({
  selector: 'app-danhsachghe',
  template: `
    <div class="container bg-light text-center">
      <h2 class="text-warning">ĐẶT VÉ XE BUS HÃNG CYBERSOFT</h2>
      <div class="row m-auto w-50">
        <div class="col-6">
          <div class="bg-secondary text-center rounded py-1">Tài xế</div>
          <div class="row">
            <div class="col-3" *ngFor="let ghe of DanhSachGhe">
              {{ handleStatusGhe(ghe) }}
              <app-ghe
                [statusGhe]="statusGhe"
                [ghe]="ghe"
                (emitStatusGhe)="DatGheParent($event, ghe)"
              ></app-ghe>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="bg-light rounded">
            <div class="text-warning">
              Danh sách ghế đã đặt ({{ soGheDaDat }})
            </div>
            <div *ngFor="let gheDaDat of DanhSachGheDangDat">
              <span>
                Ghế: {{ gheDaDat.TenGhe }} - {{ gheDaDat.Gia }}$
                <button
                  (click)="deleteGhe(gheDaDat)"
                  class="text-danger border-0 bg-transparent"
                >
                  [Hủy]
                </button>
              </span>
            </div>
            <p class="mb-0">Tổng tiền: {{ calcTotal() }}$</p>
            <p class="text-warning">Số ghế còn lại: {{ soGheConLai }}</p>
          </div>
        </div>
      </div>
    </div>
  `,
})
export class DanhSachGheComponent implements OnInit {
  @ViewChild(GheComponent) tagButtonGhe!: GheComponent;

  DanhSachGhe: {
    SoGhe: number;
    TenGhe: string;
    Gia: number;
    TrangThai: boolean;
  }[] = [
    { SoGhe: 1, TenGhe: 'số 01', Gia: 100, TrangThai: false },
    { SoGhe: 2, TenGhe: 'số 02', Gia: 200, TrangThai: false },
    { SoGhe: 3, TenGhe: 'số 03', Gia: 300, TrangThai: false },
    { SoGhe: 4, TenGhe: 'số 04', Gia: 400, TrangThai: false },
    { SoGhe: 5, TenGhe: 'số 05', Gia: 500, TrangThai: false },
    { SoGhe: 6, TenGhe: 'số 06', Gia: 600, TrangThai: false },
    { SoGhe: 7, TenGhe: 'số 07', Gia: 700, TrangThai: false },
    { SoGhe: 8, TenGhe: 'số 08', Gia: 800, TrangThai: false },
    { SoGhe: 9, TenGhe: 'số 09', Gia: 900, TrangThai: false },
    { SoGhe: 10, TenGhe: 'số 10', Gia: 100, TrangThai: false },
    { SoGhe: 11, TenGhe: 'số 11', Gia: 100, TrangThai: false },
    { SoGhe: 12, TenGhe: 'số 12', Gia: 100, TrangThai: false },
    { SoGhe: 13, TenGhe: 'số 13', Gia: 100, TrangThai: false },
    { SoGhe: 14, TenGhe: 'số 14', Gia: 100, TrangThai: false },
    { SoGhe: 15, TenGhe: 'số 15', Gia: 100, TrangThai: false },
    { SoGhe: 16, TenGhe: 'số 16', Gia: 100, TrangThai: false },
    { SoGhe: 17, TenGhe: 'số 17', Gia: 100, TrangThai: false },
    { SoGhe: 18, TenGhe: 'số 18', Gia: 100, TrangThai: false },
    { SoGhe: 19, TenGhe: 'số 19', Gia: 100, TrangThai: false },
    { SoGhe: 20, TenGhe: 'số 20', Gia: 100, TrangThai: false },
    { SoGhe: 21, TenGhe: 'số 21', Gia: 100, TrangThai: false },
    { SoGhe: 22, TenGhe: 'số 22', Gia: 100, TrangThai: false },
    { SoGhe: 23, TenGhe: 'số 23', Gia: 100, TrangThai: false },
    { SoGhe: 24, TenGhe: 'số 24', Gia: 100, TrangThai: false },
    { SoGhe: 25, TenGhe: 'số 25', Gia: 100, TrangThai: false },
    { SoGhe: 26, TenGhe: 'số 26', Gia: 100, TrangThai: false },
    { SoGhe: 27, TenGhe: 'số 27', Gia: 100, TrangThai: false },
    { SoGhe: 28, TenGhe: 'số 28', Gia: 100, TrangThai: false },
    { SoGhe: 29, TenGhe: 'số 29', Gia: 100, TrangThai: false },
    { SoGhe: 30, TenGhe: 'số 30', Gia: 100, TrangThai: true },
    { SoGhe: 31, TenGhe: 'số 31', Gia: 100, TrangThai: false },
    { SoGhe: 32, TenGhe: 'số 32', Gia: 100, TrangThai: false },
    { SoGhe: 33, TenGhe: 'số 33', Gia: 100, TrangThai: false },
    { SoGhe: 34, TenGhe: 'số 34', Gia: 100, TrangThai: false },
    { SoGhe: 35, TenGhe: 'số 35', Gia: 100, TrangThai: false },
    { SoGhe: 36, TenGhe: 'số 36', Gia: 100, TrangThai: true },
  ];

  statusGhe: boolean = false;
  handleStatusGhe(ghe: Ghe) {
    this.statusGhe =
      this.DanhSachGheDangDat.findIndex((i) => {
        return i.SoGhe === ghe.SoGhe;
      }) !== -1;
  }
  soGheDaDat: number = 0;
  soGheConLai: number = 0;
  DanhSachGheDangDat: {
    SoGhe: number;
    TenGhe: string;
    Gia: number;
    TrangThai: boolean;
  }[] = [];
  constructor() {}

  ngOnInit() {
    for (let item of this.DanhSachGhe) {
      if (!item.TrangThai) {
        this.soGheConLai++;
      }
    }
  }

  DatGheParent(status: boolean, ghe: Ghe) {
    if (status) {
      this.soGheDaDat++;
      this.soGheConLai--;
      this.DanhSachGheDangDat = [...this.DanhSachGheDangDat, ghe];
    } else {
      this.soGheDaDat--;
      this.soGheConLai++;
      let foundIndex = this.DanhSachGheDangDat.findIndex((item: Ghe) => {
        return item.SoGhe === ghe.SoGhe;
      });
      this.DanhSachGheDangDat.splice(foundIndex, 1);
    }
    console.log(this.DanhSachGheDangDat);
    console.log(status, ghe);
  }

  deleteGhe(gheDaDat: Ghe) {
    this.soGheDaDat--;
    this.soGheConLai++;
    let foundIndex = this.DanhSachGheDangDat.findIndex((item: Ghe) => {
      return item.SoGhe === gheDaDat.SoGhe;
    });
    this.DanhSachGheDangDat.splice(foundIndex, 1);
  }

  calcTotal() {
    return this.DanhSachGheDangDat.reduce((total, item) => {
      return (total += item.Gia);
    }, 0);
  }
}

interface Ghe {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
