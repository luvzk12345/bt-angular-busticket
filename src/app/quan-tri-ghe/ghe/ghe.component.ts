import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ghe',
  template: `
    <ng-container *ngIf="!ghe.TrangThai; else tagGheDaDat">
      <div
        class="bg-success rounded py-1 my-2"
        [ngClass]="{ 'bg-dark': statusGhe }"
      >
        <button
          (click)="datGhe()"
          class="border-0 bg-transparent text-white w-100"
        >
          {{ ghe.SoGhe }}
        </button>
      </div>
    </ng-container>
    <ng-template #tagGheDaDat>
      <div class="bg-danger rounded py-1 my-2">
        <button
          (click)="datGhe()"
          class="border-0 bg-transparent text-white w-100"
          disabled
        >
          {{ ghe.SoGhe }}
        </button>
      </div>
    </ng-template>
  `,
})
export class GheComponent implements OnInit {
  @Input() ghe!: Ghe;
  @Input() statusGhe!: boolean;
  @Output() emitStatusGhe = new EventEmitter();

  datGhe() {
    this.statusGhe = !this.statusGhe;
    this.emitStatusGhe.emit(this.statusGhe);
  }

  constructor() {}

  ngOnInit() {}
}

interface Ghe {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
